import { TestBed } from '@angular/core/testing';

import { ParanaFileImplService } from './parana-file-impl.service';

describe('ParanaFileImplService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ParanaFileImplService = TestBed.get(ParanaFileImplService);
    expect(service).toBeTruthy();
  });
});
